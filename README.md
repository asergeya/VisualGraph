# VisualGraph

Visualization for some algorithms on graphs.

- [x] BFS
- [x] Coloring

## Using

### Requirements

1. Python 3
2. Pygame

### Preparation

You need to have a file with coordinates for each graph's node.
Use `python graph_drawer.py` to get this.

Also, you need an adjacent table for this graph.
You can check if a table is valid with: `python check_adjacent_table.py name_of_table`

### Running

Visualization: `python main.py`
