class Node:
    how_many = 0

    def __init__(self, adjacents, coordinates=None, line_color='grey40', fill_color='black'):
        self.number = Node.how_many
        self.adjacents = adjacents
        self.coordinates = coordinates
        self.lcolor = line_color
        self.fcolor = fill_color

        Node.how_many += 1

    def get_adjacents(self):
        return [x for x in self.adjacents if x < self.number]

class Edge:
    def __init__(self, n1, n2, weight=0, color='grey40'):
        self.n1 = min(n1, n2);
        self.n2 = max(n1, n2);
        self.weight = weight
        self.color = color

def edge_id(n1, n2): return tuple(sorted((n1, n2)))

class Graph:
    order = 0

    def __init__(self, nodes=None):
        if nodes == None:
            nodes = []
        self.nodes = nodes
        Graph.order = len(nodes)

    def build_edges(self):
        self.edges = {}
        for n1 in self.nodes:
            for n2 in n1.adjacents:
                e = Edge(n1.number, n2)
                if e not in self.edges:
                    self.edges[edge_id(n1.number, n2)] = e

    def load_from_file(self, file_name="graph.txt"):
        with open(file_name) as f:
            Graph.order = int(f.readline())
            for i in range(Graph.order):
                x, y = [int(i) for i in next(f).split()]
                adjacents = [int(x) for x in f.readline().split()]
                self.nodes.append(Node(tuple(adjacents), (x, y)))

    def load_from_adjacent_table(self, file_name="adjacent_table.txt"):
        with open(file_name) as f:
            Graph.order = int(f.readline())
            self.nodes = []
            self.edges = {}
            for i in range(Graph.order):
                line = f.readline().split()
                adjacents = []
                for n, j in enumerate(line):
                    if int(j) != 0:
                        adjacents.append(n)
                        e = Edge(i, n, int(j))
                        if e not in self.edges:
                            self.edges[edge_id(i, n)] = e
                self.nodes.append(Node(tuple(adjacents)))

    def add_coordinates(self):
        with open(f"layouts/{Graph.order}.txt") as f:
            for i in range(Graph.order):
                x, y = [int(i) for i in next(f).split()]
                self.nodes[i].coordinates = (x, y)

    @classmethod
    def get_order(cls):
        return cls.order

def load_graph_from_file(file_name="graph.txt"):
    with open(file_name) as f:
        Graph.order = int(f.readline())
        nodes = []
        for i in range(Graph.order):
            x, y = [int(i) for i in next(f).split()]
            adjacents = [int(x) for x in f.readline().split()]
            nodes.append(Node(tuple(adjacents), (x, y)))
    return nodes
