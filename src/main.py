import coloring
import bfs

choice = input("Enter:" +
               "\n\t1 for running coloring algorithm" +
               "\n\t2 for running bfs algorithm\n")

if choice == '1': coloring.run()
if choice == '2': bfs.run()
