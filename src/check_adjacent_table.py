import sys

if len(sys.argv) < 2:
    print("Usage: ./check_adjacent_table.py FILENAME")
    exit(1)

filename = sys.argv[1]
with open(filename) as f:
    n = int(f.readline())
    if n < 1:
        print("Mistake!\n",
              "Order of graph must be positive number.",
             f"(Your input is {n})")
        exit(1)

    array = []
    for i in range(n):
        array.append([int(j) for j in f.readline().split()])
    for i in range(n):
        for j in range(i+1, n):
            if array[i][j] != array[j][i]:
                print(f"Mistake!\n",
                      f"[{i}][{j}] != [{j}][{i}] ({array[i][j]} != {array[j][i]})\n")
