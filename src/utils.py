import pygame

def circle_fill(window, xy, line_color, fcolor, radius, thickness):
    pygame.draw.circle(window, line_color, xy, radius)
    pygame.draw.circle(window, fcolor, xy, radius - thickness)

def write_text(window, text, coordinates, font_size, color='white'):
    font = pygame.font.Font(None, font_size)
    text = font.render(text, True, color)
    text_rect = text.get_rect(center=coordinates)
    window.blit(text, text_rect)
