# TODO: нужно полностью все переписать
import pygame
from graph import Graph
from utils import circle_fill, write_text

size = (width, height) = (1920, 1000)
speed = 6
pause_bad_color = True
radius = 25
thickness = 2
font_size = 35
colors = ('grey40',     'darkred',        'darkblue',     'darkgreen',
          'purple',     'tomato',         'violet',       'violetred',
          'darkorange', 'darkgoldenrod',  'darkseagreen', 'darkmagenta',
          'darkcyan',   'darkolivegreen', 'darkorchid',   'darksalmon',
          'darkviolet', 'darkturquoise',  'darkslateblue','darkkhaki',
          'springgreen','steelblue',      'sienna',       'rosybrown',
          'royalblue',  'thistle',        'tan',          'plum',
          'maroon',     'navy',           'red',          'blue',
          'green')

def run():
    global clock
    # Нужна для хранения правильной раскраски
    global work_color
    # TODO: Вынести в отдельный модуль
    graph = Graph()
    graph.load_from_adjacent_table()
    graph.add_coordinates()

    pygame.init()
    clock = pygame.time.Clock()
    screen = pygame.display.set_mode(size)

    draw_graph(screen, graph)

    color = [0] * graph.get_order()

    primary_coloring(screen, graph, color)
    improving_coloring(screen, graph, color)
    write_text(screen, f"q = {max(color)}", (width / 2, height-50), 50)
    pygame.time.wait(1000)
    # Раскрасить в правильную раскраску
    # Начало
    color = work_color
    for c, i in enumerate(graph.nodes):
        i.fcolor = colors[color[c]]
    draw_graph(screen, graph)
    # Конец
    pygame.display.update()
    #pygame.time.wait(60000)
    # TODO: закрытие по нажатию клавиши
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    running = False

def is_safe(node, color, c):
    """Проверяет можно ли раскрасить вершину в цвет c """
    for i in node.get_adjacents():
        if color[i] == c:
            return False
    return True

def paint(window, graph, start, color, q, c=1):
    """Раскрашиваем граф с вершины start"""

    for i in range(start, graph.get_order()):
        # Выделение вершины для закраски
        graph.nodes[i].lcolor = 'white'
        update_graph(window, graph)
        # Выбор цвета для заливки
        while not is_safe(graph.nodes[i], color, c):
            c += 1
        if c >= q:
            graph.nodes[i].fcolor = colors[c]
            update_graph(window, graph)
            if pause_bad_color:
                pygame.time.wait(1000)
            graph.nodes[i].lcolor = colors[0] # 'grey40'
            update_graph(window, graph)
            return q, i;
        color[i] = c
        c = 1
        # Заливка + снятие выделения
        graph.nodes[i].fcolor = colors[color[i]]
        graph.nodes[i].lcolor = colors[0] # 'grey40'
        update_graph(window, graph)
    # Заносится правильная раскраска
    global work_color
    work_color = color[:]
    #
    q = max(color)
    return q, color.index(q)

def primary_coloring(window, graph, color):
    paint(window, graph, 0, color, graph.get_order()+1)
    print(color)

def improving_coloring(window, graph, color):
    # Кол-во использованных цветов
    q = max(color)
    # Номер вершины
    xi = color.index(q)
    while xi != 1 and graph.nodes[xi].get_adjacents():
        x = max(graph.nodes[xi].get_adjacents())
        new_color = color[x] + 1
        while new_color < q and not is_safe(graph.nodes[x], color, new_color):
            new_color += 1
        if new_color != q:
            q, x = paint(window, graph, x, color, q, new_color)
            #q = max(color)
            #x = color.index(q)
            print(color)
        xi = x

def draw_graph(window, graph):
    # Рисует ребра
    for e in graph.edges.values():
        pygame.draw.line(window, e.color, graph.nodes[e.n1].coordinates, graph.nodes[e.n2].coordinates, 2)
    # Рисует вершины
    for i in graph.nodes:
        circle_fill(window, i.coordinates, i.lcolor, i.fcolor, radius, thickness)
        write_text(window, str(i.number), i.coordinates, font_size)

    clock.tick(speed)
    pygame.display.update()

def update_graph(window, graph):
    draw_graph(window, graph)
