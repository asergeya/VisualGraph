import pygame

def save_in_file(n, coordinates):
    with open(f"{n}.txt", "w") as f:
        for i in coordinates:
            f.write(f"{i[0]} {i[1]}\n")

size = (width, height) = (1920, 1000)
pygame.init()
clock = pygame.time.Clock()
screen = pygame.display.set_mode(size)
screen.fill('black')
color = 'green'
radius = 25

nodes_num = 0
coordinates = []

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
                pygame.draw.circle(screen, color, event.pos, radius)
                nodes_num += 1
                coordinates.append(event.pos)
                print(event.pos[0], event.pos[1])
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_s:
                save_in_file(nodes_num, coordinates)
            elif event.key == pygame.K_q:
                running = False

    pygame.display.flip()
    clock.tick(60)
