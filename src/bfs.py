import pygame
from graph import Graph, edge_id
from utils import circle_fill, write_text

# FIXME: colors
display_width = 1920
display_height = 1000
radius = 30
speed = 5

grey = 'grey40'
white = 'white'
yellow = 'darkred'
red = 'darkblue'
black = 'black'
blue = 'darkgreen'

def run():
    global clock
    graph = Graph()
    graph.load_from_adjacent_table()
    graph.add_coordinates()

    pygame.init()
    clock = pygame.time.Clock()
    screen = pygame.display.set_mode((display_width, display_height))

    draw_graph(screen, graph)
    # Алгоритм BFS
    # TODO: add numbers
    queue = [0]
    while len(queue) > 0:
        n1 = queue.pop(0)
        current = graph.nodes[n1]
        current.lcolor = white
        current.fcolor = yellow
        update_graph(screen, graph)
        for n2 in current.adjacents:
            if graph.nodes[n2].fcolor == black and n2 not in queue:
                queue.append(n2)
                graph.nodes[n2].lcolor = white
                graph.nodes[n2].fcolor = red
                graph.edges[edge_id(n1,n2)].color = white
                update_graph(screen, graph)
        current.fcolor = blue
        update_graph(screen, graph)

    pygame.time.wait(5000)

def draw_graph(window, graph):
    for e in graph.edges.values():
        pygame.draw.line(window, e.color, graph.nodes[e.n1].coordinates, graph.nodes[e.n2].coordinates, 2)

    for i in graph.nodes:
        circle_fill(window, i.coordinates, i.lcolor, i.fcolor, 25, 2)

    pygame.display.update()
    clock.tick(speed)

def update_graph(window, graph):
    for e in graph.edges.values():
        if e.color == white:
            pygame.draw.line(window, e.color, graph.nodes[e.n1].coordinates, graph.nodes[e.n2].coordinates, 2)

    for i in graph.nodes:
        circle_fill(window, i.coordinates, i.lcolor, i.fcolor, 25, 2)

    pygame.display.update()
    clock.tick(speed)
